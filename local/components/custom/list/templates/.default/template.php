<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?php foreach($arResult['ELEMENTS'] as $arElement) : ?>
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title"><?=$arElement['NAME']?></h3>
        </div>
        <div class="panel-body">
            <div class="users">
                <b>Привязанные пользователи:</b>
                <?php foreach($arElement['USERS'] as $user) : ?>
                    <div class="users-single">
                        <?=$user?>
                    </div>
                <?php endforeach; ?>
            </div>
            <hr/>
            <div class="elements">
                <b>Привязанные элементы инфоблока:</b>
                <?php foreach($arElement['INFO_ID'] as $info) : ?>
                    <div class="elements-single">
                        <?=$info?>
                    </div>
                <?php endforeach; ?>
            </div>
            <hr/>
            <div class="date">
                <b>Последняя дата изменения: </b><?=$arElement['TIMESTAMP_X']?>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<div class="nav">
    <?=$arResult['NAV_STRING']?>
</div>
