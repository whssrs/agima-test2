<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
//INFO prop by code
//cache?
//template
class CCustomList extends CBitrixComponent 
{                   
    private $arUserID;

    public function onPrepareComponentParams($arParams)
    {
        $result = [
            'PAGE_SIZE'           => intval($arParams['PAGE_SIZE']),
            'PAGER_TEMPLATE'      => $arParams['PAGER_TEMPLATE'],
            'CACHE_TYPE'          => $arParams['CACHE_TYPE'],
            'CACHE_TIME'          => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"]: 36000000,
            'ELEMENT_IBLOCK_CODE' => $arParams['ELEMENT_IBLOCK_CODE'],
            'INFO_IBLOCK_CODE'    => $arParams['INFO_IBLOCK_CODE'],
            'LIST_NAME'           => htmlspecialcharsEx($arParams['LIST_NAME']),
        ];

        return $result;
    }

    public function executeComponent()
    {
	    $arNavigation = CDBResult::GetNavParams($arNavParams);
        if($this->startResultCache(false, [$arNavigation]))
        {
            \Bitrix\Main\Loader::includeModule('iblock');
            $this->executeMain();
            $this->includeComponentTemplate();
        }
    }

    private function executeMain()
    {
        $this->queryElements();
        $this->queryUsers();
    }

    private function queryElements()
    {
        $this->arUserID = [];
        $obElements = CIBlockElement::GetList(
            [],
            [
                'IBLOCK_CODE' => $this->arParams['ELEMENT_IBLOCK_CODE'],
                'PROPERTY_LIST_VALUE' => $this->arParams['LIST_NAME'],
            ],
            false,
            [
                'nPageSize' => $this->arParams['PAGE_SIZE']
            ],
            [
                'ID', 'NAME', 'IBLOCK_ID', 'TIMESTAMP_X'
            ]
        );

        while($obElement = $obElements->GetNextElement()) {
            $arProps = $obElement->GetProperties();
            $arFields = $obElement->GetFields();
            $arFields['INFO_ID'] = $arProps['INFO']['VALUE'];
            $arFields['USER_ID'] = $arProps['USERS']['VALUE'];
            $this->arResult['ELEMENTS'][] = $arFields;
            foreach($arFields['USER_ID'] as $iUserID)
            {
                if(!in_array($iUserID, $this->arUserID))
                    $this->arUserID[] = $iUserID;
            }
        }
        $this->arResult['NAV_STRING'] = $obElements->GetPageNavStringEx($navComponentObject, '', $this->arParams['PAGER_TEMPLATE']);
    }

    private function queryUsers()
    {
        $by = 'timestamp_x';
        $order = 'desc';
        $sIdFilter = implode(" | ", $this->arUserID);
        $obUsers = CUser::GetList( 
            $by,
            $order,
            [
                'ID' => $sIdFilter
            ],
            [
                'FIELDS' => ["NAME", "LAST_NAME", "SECOND_NAME", "ID"]
            ]
        );
        while($arUser = $obUsers->GetNext()) 
        {
            $arUsers[$arUser['ID']] = $arUser;
        }
        foreach($this->arResult['ELEMENTS'] as &$arElement) 
        {
            foreach($arElement['USER_ID'] as $iUserID)
            {
                $currentUser = $arUsers[$iUserID];
                $arElement['USERS'][] = $currentUser['LAST_NAME'] . " " . $currentUser['NAME'] . " " . $currentUser['SECOND_NAME'];
            }
        }
    }
}
