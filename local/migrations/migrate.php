<?php

define("NO_KEEP_STATISTIC", true);
define("NO_AGENT_CHECK", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

                       
$arParamsIblockType = [
    'ID'       => 'basic',
    'SECTIONS' => 'N',
    'IN_RSS'   => 'N',
    'SORT'     => 500,
    'LANG'     => [
        'ru' => [
            'NAME'         => 'Базовый',
            'SECTION_NAME' => 'Базовый',
            'ELEMENT_NAME' => 'Базовый',
        ],
        'en' => [
            'NAME'         => 'Basic',
            'SECTION_NAME' => 'Basic',
            'ELEMENT_NAME' => 'Basic',
        ],
    ]
];

$arParamsIblock[] = [
    'ACTIVE' => 'Y',
    'NAME' => 'Элементы списка',
    'IBLOCK_TYPE_ID' => 'basic',
    'CODE' => 'LIST_ELEMENT',
    'SITE_ID' => ['s1'],
    'SORT' => 500,
    'GROUP_ID' => [1 => 'R'],
    'LIST_MODE' => 'C',
    'WORKFLOW' => 'N',
    'INDEX_ELEMENT' => 'N',
    'INDEX_SECTION' => 'N',
    'RSS_ACTIVE' => 'N',
    'XML_ID' => 'LIST_ELEMENT',
    'LIST_PAGE_URL' => '/#IBLOCK_CODE#/',
    'SECTION_PAGE_URL' => '/#IBLOCK_CODE#/#SECTION_CODE_PATH#/',
    'DETAIL_PAGE_URL' => '/#IBLOCK_CODE#/#SECTION_CODE_PATH#/#ELEMENT_CODE#.php',
    'FIELDS' => [
        'ACTIVE_FROM' => [
            'IS_REQUIRED' => 'N',
            'DEFAULT_VALUE' => '=now',
        ],
        'ACTIVE_TO' => [
            'IS_REQUIRED' => 'N',
        ],
        'DETAIL_PICTURE' => [
            'IS_REQUIRED' => 'N',
        ],
        'CODE' => [
            'IS_REQUIRED' => 'N',
            'DEFAULT_VALUE' => [
                'UNIQUE' => 'Y',
            ],
        ],
        'IBLOCK_SECTION' => [
            'IS_REQUIRED' => 'N',
        ],
        'SECTION_CODE' => [
            'IS_REQUIRED' => 'N',
            'DEFAULT_VALUE' => [
                'TRANSLITERATION' => 'Y',
                'UNIQUE' => 'Y',
                'TRANS_CASE' => 'L',
                'TRANS_SPACE' => '-',
                'TRANS_OTHER' => '-',
            ],
        ],
    ],
    'IS_CATALOG' => 'N',
    'VAT_ID' => '',
];

$arParamsProps = [
    [
        'NAME'           => 'Список',
        'ACTIVE'         => 'Y',
        'SORT'           => 500,
        'CODE'           => 'LIST',
        'IBLOCK_ID'      => '#IBLOCK_ID#',
        'PROPERTY_TYPE'  => 'L',
        'IS_REQUIRED'    => 'N',
        'FILTRABLE'      => 'Y',
        'MULTIPLE'       => 'N',
	],
    [
        'NAME'           => 'Пользователи',
        'ACTIVE'         => 'Y',
        'SORT'           => 500,
        'CODE'           => 'USERS',
        'IBLOCK_ID'      => '#IBLOCK_ID#',
        'PROPERTY_TYPE'  => 'S',
        'USER_TYPE'      => 'UserID',
        'IS_REQUIRED'    => 'N',
        'FILTRABLE'      => 'Y',
        'MULTIPLE'       => 'Y',
	],
    [                                                                   
        'NAME'           => 'Информация',
        'ACTIVE'         => 'Y',
        'SORT'           => 500,
        'CODE'           => 'INFO',
        'IBLOCK_ID'      => '#IBLOCK_ID#',
        'LINK_IBLOCK_ID' => '#LINK_IBLOCK_ID#',
        'PROPERTY_TYPE'  => 'E',
        'IS_REQUIRED'    => 'N',
        'FILTRABLE'      => 'Y',
        'MULTIPLE'       => 'Y',
	],
];

$arParamsPropEnum = [
    [
        'PROPERTY_ID' => '#PROP_ID#',
        'VALUE'       => 'Главный список',
        'XML_ID'      => 'MAIN',
    ],
    [
        'PROPERTY_ID' => '#PROP_ID#',
        'VALUE'       => 'Дополнительный список',
        'XML_ID'      => 'ADDITIONAL',
    ],
];

$arParamsIblock[1] = $arParamsIblock[0];
$arParamsIblock[1]['NAME'] = "Информация об элементах";
$arParamsIblock[1]['CODE'] = "INFO_ELEMENT";
$arParamsIblock[1]['XML_ID'] = "INFO_ELEMENT";

$arNames = ["Сергей", "Иван", "Николай", "Кирилл", "Петр"];
$arLastNames = ["Иванов", "Петров", "Сидоров", "Кузнецов", "Васильев"];
$arSecondNames = ["Сергеевич", "Иванович", "Николаевич", "Владимирович", "Васильевич"];


function sGetMessage($key, $fields)
{
    $messages = [
        'NEW_IBLOCK_TYPE_ADDED'        => 'Тип информационного блока #IBLOCK_TYPE# успешно добавлен',
        'NEW_IBLOCK_TYPE_UPDATED'      => 'Тип информационного блока #IBLOCK_TYPE# успешно обновлен',
        'NEW_IBLOCK_TYPE_ERROR_ADD'    => 'Возникла ошибка при добавлении #IBLOCK_TYPE#: #ERROR#',
        'NEW_IBLOCK_TYPE_ERROR_UPDATE' => 'Возникла ошибка при обновлении #IBLOCK_TYPE#: #ERROR#',
        
        'NEW_IBLOCK_ADDED'             => 'Информационный блок #IBLOCK# успешно добавлен',
        'NEW_IBLOCK_UPDATED'           => 'Информационный блок #IBLOCK# успешно обновлен',
        'NEW_IBLOCK_ERROR_ADD'         => 'Возникла ошибка при добавлении #IBLOCK#: #ERROR#',
        'NEW_IBLOCK_ERROR_UPDATE'      => 'Возникла ошибка при обновлении #IBLOCK#: #ERROR#',

        'NEW_PROP_ADDED'               => 'Свойство #NAME# успешно добавлено',
        'NEW_PROP_UPDATED'             => 'Свойство #NAME# успешно обновлено',
        'NEW_PROP_ERROR_ADD'           => 'Возникла ошибка при добавлении свойства #NAME#: #ERROR#',
        'NEW_PROP_ERROR_UPDATE'        => 'Возникла ошибка при обновлении свойства #NAME#: #ERROR#',

        'NEW_USER_ERROR_ADD'           => 'Возникла ошибка при добавлении пользователя: #ERROR#',

        'NEW_ELEMENT_ERROR_ADD'           => 'Возникла ошибка при добавлении элемента: #ERROR#',
    ];
                                                              
    return isset($messages[$key]) ? str_replace(array_keys($fields), array_values($fields), $messages[$key]) : '';
}

global $DB;

$DB->StartTransaction();

try {
    \Bitrix\Main\Loader::includeModule('iblock');

    $info = [];

    // Добавление нового типа инфоблока
    $bIblockTypeExists = false;

    $obIblockTypeDB = \Bitrix\Iblock\TypeTable::getById($arParamsIblockType['ID']);

    if($obIblockType = $obIblockTypeDB->fetch()) 
    {
        $bIblockTypeExists = true;
    }
    $obIblockType = new CIBlockType();

    if($bIblockTypeExists) 
    {
        if($obIblockType->Update($arParamsIblockType['ID'], $arParamsIblockType)) 
        {
            $info[] = sGetMessage('NEW_IBLOCK_TYPE_UPDATED', [
                '#IBLOCK_TYPE#' => $arParamsIblockType['ID']
            ]);
        } 
        else 
        {
            throw new \Bitrix\Main\SystemException(sGetMessage('NEW_IBLOCK_TYPE_ERROR_UPDATE', [
                '#IBLOCK_TYPE#' => $arParamsIblockType['ID'],
                '#ERROR#' => $obIblockType->LAST_ERROR
            ]));
        }
    }
    else 
    {
        $res = $obIblockType->add($arParamsIblockType);
        if($res) 
        {
            $info[] = sGetMessage('NEW_IBLOCK_TYPE_ADDED', [
                '#IBLOCK_TYPE#' => $arParamsIblockType['ID']
            ]);
        }
        else 
        {
            throw new \Bitrix\Main\SystemException(sGetMessage('NEW_IBLOCK_TYPE_ERROR_ADD', [
                '#IBLOCK_TYPE#' => $arParamsIblockType['ID'],
                '#ERROR#' => $obIblockType->LAST_ERROR
            ]));
        }
    }

    //Добавление инфоблоков
    foreach($arParamsIblock as $key => $arIblock) 
    {
        $iblockId = 0;
        $obIblockDB = \Bitrix\Iblock\IblockTable::getList([
            'filter' => [
                '=CODE' => $arIblock['CODE']
            ]
        ]);
        if ($obIblock = $obIblockDB->fetch()) 
        {
            $iblockId = $obIblock['ID'];
        }

        $obIblock = new CIBlock();

        if(intval($iblockId) > 0) 
        {
            if($obIblock->Update($iblockId, $arIblock)) 
            {
                $info[] = sGetMessage('NEW_IBLOCK_UPDATED', [
                    '#IBLOCK#' => $arIblock['NAME']
                ]);
            }
            else 
            {
                throw new \Bitrix\Main\SystemException(sGetMessage('NEW_IBLOCK_ERROR_UPDATE', [
                    '#IBLOCK#' => $arIblock['NAME'],
                    '#ERROR#' => $obIblock->LAST_ERROR
                ]));
            }
        } 
        else 
        {
            $iblockId = $obIblock->add($arIblock);
            if($iblockId) 
            {
                $info[] = sGetMessage('NEW_IBLOCK_ADDED', [
                    '#IBLOCK#' => $arIblock['NAME']
                ]);
            } 
            else 
            {
                throw new \Bitrix\Main\SystemException(sGetMessage('NEW_IBLOCK_ERROR_ADD', [
                    '#IBLOCK#' => $arIblock['NAME'],
                    '#ERROR#' => $obIblock->LAST_ERROR
                ]));
            }
        }
        $arIblockId[] = $iblockId;
    }

    //Добавление свойств инфоблока
    foreach($arParamsProps as $arProp) 
    {
        $arProp['IBLOCK_ID'] = str_replace('#IBLOCK_ID#', $arIblockId[0], $arProp['IBLOCK_ID']);
        $arProp['LINK_IBLOCK_ID'] = str_replace('#LINK_IBLOCK_ID#', $arIblockId[1], $arProp['LINK_IBLOCK_ID']);

        $obProp = new CIBlockProperty();
        
        $resProp = CIBlockProperty::GetList(
            [],
            [
                'CODE' => $arProp['CODE'],
                'IBLOCK_ID' => $arProp['IBLOCK_ID'],
            ]
        );


        if($arExistProp = $resProp->Fetch()) 
        {
            $iPropID = $arExistProp['ID'];
            if($obProp->Update($arExistProp['ID'], $arProp)) 
            {
                $info[] = sGetMessage('NEW_PROP_UPDATED', [
                    '#NAME#' => $arProp['NAME']
                ]);           
            }
            else
            {
                if($ex = $APPLICATION->GetException()) 
                {
                    $error = $ex->GetString();
                }
                else
                {
                    $error = $obProp->LAST_ERROR;
                }
                throw new \Bitrix\Main\SystemException(sGetMessage('NEW_PROP_ERROR_UPDATE', [
                    '#ERROR#' => $error,
                    '#NAME#' => $arProp['NAME'],
                ]));
            }                      
        }
        else
        {
            $iPropID = $obProp->Add($arProp);
            if($iPropID) 
            {
                $info[] = sGetMessage('NEW_PROP_ADDED', [
                    '#NAME#' => $arProp['NAME']
                ]);           
            }
            else
            {
                if($ex = $APPLICATION->GetException()) 
                {
                    $error = $ex->GetString();
                }
                else
                {
                    $error = $obProp->LAST_ERROR;
                }
                throw new \Bitrix\Main\SystemException(sGetMessage('NEW_PROP_ERROR_ADD', [
                    '#ERROR#' => $error,
                    '#NAME#' => $arProp['NAME'],
                ]));
            }
        }
        // Добавление вариантов значиний свойства типа "список"
        if($arProp['CODE'] == 'LIST')
        {
            $obPropEnum = CIBlockPropertyEnum::getList(
                [],
                [
                    'IBLOCK_ID' => $arProp['IBLOCK_ID'],
                    'CODE' => $arProp['CODE'],
                ]
            );

            while ($prop = $obPropEnum->fetch()) 
            {
                $arPropEnum[$prop['XML_ID']] = $prop;
            }

            foreach($arParamsPropEnum as $propEnum) 
            {
                if (!array_key_exists( $propEnum['XML_ID'], $arPropEnum))
                {
                    $propEnum['PROPERTY_ID'] = str_replace('#PROP_ID#', $iPropID, $propEnum['PROPERTY_ID']);
                    $obEnum = new CIBlockPropertyEnum();
                    $iEnumID = $obEnum->add($propEnum);
                    $propEnum['ID'] = $iEnumID;
                    $arPropEnum[$propEnum['XML_ID']] = $propEnum;
                } 
            }
        }
    }            


    //Добавление случайно сгенерированных пользователей
    if(CUser::GetCount() < 21)
    {
        $info[] = "Добавление пользователей:";
        for($i = 0; $i < 20; $i++) 
        {
            $obUser = new CUser();
            $arUserFields = [
                'LOGIN' => 'testing' . $i,
                'EMAIL' => 'testing' . $i . '@test.test',
                'PASSWORD' => '123123',
                'PASSWORD' => '123123',
                'NAME' => $arNames[rand() % 5],
                'LAST_NAME' => $arLastNames[rand() % 5],
                'SECOND_NAME' => $arSecondNames[rand() % 5],
            ];
            $iUserID = $obUser->Add($arUserFields);
            if(!(intval($iUserID) > 0))
            {
                throw new \Bitrix\Main\SystemException(sGetMessage('NEW_USER_ERROR_ADD', [
                    '#ERROR#' => $obUser->LAST_ERROR,
                ]));
            }
        }
        $info[] = "Пользователи успешно добавлены";
    }
    else
    {
        $info[] = "Добавление пользователей не требуется";
    }
    $by = 'timestamp_x';
    $order = 'desc';
    $obUsers = CUser::GetList(
        $by,
        $order,
        [
            '!ID' => 1,
        ],
        [
            'nTopCount' => 20,
        ]
    );
    while($arUser = $obUsers->GetNext())
    {
        $arUsers[] = $arUser;
    }
        

    //Добавление элементов инфоблока для привязки
    $iElementsCount = CIBlock::GetElementCount($arIblockId[1]);
    if($iElementsCount < 20) 
    {
        $info[] = "Добавление элементов в инфоблок {$arParamsIblock[1]['NAME']}:";

        $arFields = [
            "NAME" => "test",
            "ACTIVE" => "Y",
            "IBLOCK_ID" => $arIblockId[1],
        ];

        for ($i = 0; $i < 20; $i++)
        {
            $obElement = new CIBlockElement();
            $iElementID = $obElement->Add($arFields);
            if(!(intval($iElementID) > 0))
            {
                throw new \Bitrix\Main\SystemException(sGetMessage('NEW_ELEMENT_ERROR_ADD', [
                    '#ERROR#' => $obElement->LAST_ERROR,
                ]));
            }
        }
        $info[] = "Элементы успешно добавлены";
    }
    else
    {
        $info[] = "Добавление элементов в инфоблок {$arParamsIblock[1]['NAME']} не требуется";
    }
    $rsLinkingElements = CIBlockElement::GetList(
        [],
        [
            "IBLOCK_ID" => $arIblockId[1]
        ],
        false,
        [
            'nTopCount' => 20
        ]
    );
    while($linkingElement = $rsLinkingElements->GetNext()) 
    {
        $arLinkingElements[] = $linkingElement;
    }

    //Добавление элементов списка в основной инфоблок
    $iElementsCount = CIBlock::GetElementCount($arIblockId[0]);
    if($iElementsCount < 20) 
    {
        $info[] = "Добавление элементов в инфоблок {$arParamsIblock[0]['NAME']}:";

        for ($i = 0; $i < 20; $i++)
        {
            $arPropKeys = array_keys($arPropEnum);
            $arProps = [
                "LIST" => [
                    "VALUE" => $arPropEnum[$arPropKeys[rand() % 2]]['ID']
                ],
                "USERS" => [$arUsers[rand() % 20]['ID'], $arUsers[rand() % 20]['ID']],
                "INFO" => [$arLinkingElements[rand() % 20]['ID'], $arLinkingElements[rand() % 20]['ID']],
            ];

            $arFields = [
                "NAME" => "testing" . $i,
                "ACTIVE" => "Y",
                "IBLOCK_ID" => $arIblockId[0],
                "PROPERTY_VALUES" => $arProps,
            ];

            $obElement = new CIBlockElement();
            $iElementID = $obElement->Add($arFields);
            if(!(intval($iElementID) > 0))
            {
                throw new \Bitrix\Main\SystemException(sGetMessage('NEW_ELEMENT_ERROR_ADD', [
                    '#ERROR#' => $obElement->LAST_ERROR,
                ]));
            }
        }
        $info[] = "Элементы успешно добавлены";
    }
    else
    {
        $info[] = "Добавление элементов в инфоблок {$arParamsIblock[0]['NAME']} не требуется";
    }

    $DB->Commit();
    echo implode("<br>\n", $info);
} 
catch (\Bitrix\Main\SystemException $e)
{
    $DB->Rollback();

    echo sprintf("%s<br>\n%s",
        $e->getMessage(),
        implode("<br>\n", $info)
    );
}
