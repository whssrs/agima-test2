<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Тестовое задание №2");
?>

<div class="container">
    <div class="jumbotron">
        <?php
        $APPLICATION->IncludeComponent("custom:list","",[
            "CACHE_TIME"          => 3600,
            "CACHE_TYPE"          => "A",
            "PAGE_SIZE"           => 3,
            "ELEMENT_IBLOCK_CODE" => "LIST_ELEMENT",
            "INFO_IBLOCK_CODE"    => "INFO_ELEMENT",
            "LIST_NAME"           => "Главный список",
        ]);
        ?>
    </div>
</div>

<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
